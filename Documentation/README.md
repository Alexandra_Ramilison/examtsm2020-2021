# INVESTORS MODELING
M2FIT 2020-2021

Student 1: Ramilison Maminiaina Alexandra

Student 2: Massari Sousa Pontes

The project consists on modeling 3 types of investors according to their risk appetite:

- The defensive one who only invests in bonds, 

- The aggressive who as opposed to the aggressive, only invests in stocks,

- The mixed type who combine bonds and stocks.

The bonds can be either short term or long term, and the investors can invest in one or several stocks of the following companies: FedEx, Google, Exxon Mobile, The Coca Cola Company, Nokia, Morgan Stanley and IBM.

The goal of the project is to analyse the impact of the strategy and the portfolio composition on the return of the investor.

## Python set-up

The project is modeled using Python and Git. The Python project is composed by four folders: Code, Data, Documentation and Results. 
The file "StocksData.csv" on the Data folder contains the data about the stocks, and was extracted from Yahoo Finance and modified by our codes. 

## Bonds returns

The investment on bonds has a compounded yearly interest type of return.

This gives an investment equal to ia(1+i)^n at the end of the period n, where:

- ia: initial amount invested,
- i: interest rate of the bond, 
- n: term of the bond. 

## Stocks returns

The amount invested in stocks can give a loss or a gain based on the evolution of the stock price.
As for the bond return, there is an interest rate (can also be called return rate) which is given by the formula: (sale price - purchase price)/purchase price which corresponds to the percentage of evolution of the stock since the acquisition. 
For the stocks, depending on the unit price of the stocks, not all of the initial investment amount can be spent. So the stock return is given by: number of stocks bought * purchase price * (1+i). 

Note: in the modelisation (Part 3 and 4 of the project), it is considered that the investor kept his unspent initial budget as a return on investment. 

### Coding structure
The code is divided into five (5) files:
- Bonds where the bonds are modeled, 
- Stocks where the stocks are modeled with the plot of the evolution of every stock from our data,
- InvestorsModelisation where the three types are modeled and where 500 random investments are simulated, 
- Simulation where we modeled the different scenarios (change of initial budget, proportion of bonds and stocks, reinvestment)
- Bonus where we analysed which year was the best to invest, what stock was the more profitable in 2017 and what happens if the initial amount is centrally distributed.

## References

* [Stackoverflow](https://stackoverflow.com/) - Forum between developers to handle errors
* [Google Colab](https://colab.research.google.com/notebooks/intro.ipynb) - Used to extract the dataframe from Yahoo Finance in a fast way
* [Matplotlib](https://matplotlib.org/) - Used to find already built codes for plots
* [Investopedia](https://www.investopedia.com/) - General questions about finance


