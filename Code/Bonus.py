# Student 1: M2 FIT - Maminiaina Alexandra Ramilison
# Student 2: M2 FIT - Massari Sousa Pontes

# PART 4: SIMULATION

from InvestorsModelisation import shorttermbonds, longtermbonds, defensivereturn, aggressivereturn, mixedreturn
from Bonds import streturn, ltreturn
from Stocks import stockreturn, get_column
from datetime import date
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import datetime
import random

# Variables
STIA = 250  # Short term bonds minimum investment
STMT = 2  # Minimum term for short term bonds
STIR = 0.015  # Short term interest rate p.a.
LTIA = 1000  # Long term bonds minimum investment
LTMT = 5  # Minimum term for long term bonds
LTIR = 0.03  # Long term interest rate p.a.
ia = 5000 # Initial amount
stocklist = ['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']
year = ['2016', '2017', '2018', '2019', '2020']

# Import data file
inputdataset = os.path.abspath("../Data/StocksData.csv")
MyDataframe = pd.read_csv(inputdataset, sep=",")

# The best year for an aggressive investor depending on the yearly return
fdxreturn = []
googlreturn = []
xomreturn = []
koreturn = []
nokreturn = []
msreturn = []
ibmreturn = []

##Create the dataframe which contains the yearly return of each stock
def stockreturn(name, startofyear, endofyear):
    c = get_column(name)
    op = MyDataframe.loc[MyDataframe['Date'] == startofyear]
    end = MyDataframe.loc[MyDataframe['Date'] == endofyear]
    opening = op.iloc[0, c]  # Get the price of the stock at the beginning of the year
    closing = end.iloc[0, c]  # Get the price of the stock at the end of year
    performance = (closing - opening) / opening
    return performance

fdxreturn.append(stockreturn('FDX', '2016-01-11', '2016-12-30'))
fdxreturn.append(stockreturn('FDX', '2017-01-03', '2017-12-29'))
fdxreturn.append(stockreturn('FDX', '2018-01-02', '2018-12-31'))
fdxreturn.append(stockreturn('FDX', '2019-01-02', '2019-12-31'))
fdxreturn.append(stockreturn('FDX', '2020-01-02', '2020-12-31'))

googlreturn.append(stockreturn('GOOGL', '2016-01-11', '2016-12-30'))
googlreturn.append(stockreturn('GOOGL', '2017-01-03', '2017-12-29'))
googlreturn.append(stockreturn('GOOGL', '2018-01-02', '2018-12-31'))
googlreturn.append(stockreturn('GOOGL', '2019-01-02', '2019-12-31'))
googlreturn.append(stockreturn('GOOGL', '2020-01-02', '2020-12-31'))

xomreturn.append(stockreturn('XOM', '2016-01-11', '2016-12-30'))
xomreturn.append(stockreturn('XOM', '2017-01-03', '2017-12-29'))
xomreturn.append(stockreturn('XOM', '2018-01-02', '2018-12-31'))
xomreturn.append(stockreturn('XOM', '2019-01-02', '2019-12-31'))
xomreturn.append(stockreturn('XOM', '2020-01-02', '2020-12-31'))

koreturn.append(stockreturn('KO', '2016-01-11', '2016-12-30'))
koreturn.append(stockreturn('KO', '2017-01-03', '2017-12-29'))
koreturn.append(stockreturn('KO', '2018-01-02', '2018-12-31'))
koreturn.append(stockreturn('KO', '2019-01-02', '2019-12-31'))
koreturn.append(stockreturn('KO', '2020-01-02', '2020-12-31'))

nokreturn.append(stockreturn('NOK', '2016-01-11', '2016-12-30'))
nokreturn.append(stockreturn('NOK', '2017-01-03', '2017-12-29'))
nokreturn.append(stockreturn('NOK', '2018-01-02', '2018-12-31'))
nokreturn.append(stockreturn('NOK', '2019-01-02', '2019-12-31'))
nokreturn.append(stockreturn('NOK', '2020-01-02', '2020-12-31'))

msreturn.append(stockreturn('MS', '2016-01-11', '2016-12-30'))
msreturn.append(stockreturn('MS', '2017-01-03', '2017-12-29'))
msreturn.append(stockreturn('MS', '2018-01-02', '2018-12-31'))
msreturn.append(stockreturn('MS', '2019-01-02', '2019-12-31'))
msreturn.append(stockreturn('MS', '2020-01-02', '2020-12-31'))

ibmreturn.append(stockreturn('IBM', '2016-01-11', '2016-12-30'))
ibmreturn.append(stockreturn('IBM', '2017-01-03', '2017-12-29'))
ibmreturn.append(stockreturn('IBM', '2018-01-02', '2018-12-31'))
ibmreturn.append(stockreturn('IBM', '2019-01-02', '2019-12-31'))
ibmreturn.append(stockreturn('IBM', '2020-01-02', '2020-12-31'))

stockyearlyreturn =pd.DataFrame({'Year': year,
                                'FDX': fdxreturn,
                                'GOOGL': googlreturn,
                                'XOM': xomreturn,
                                'KO': koreturn,
                                'NOK': nokreturn,
                                'MS': msreturn,
                                'IBM':ibmreturn})

stockyearlyreturn['MAX'] = stockyearlyreturn[['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']].max(axis=1)
stockyearlyreturn['MEAN'] = stockyearlyreturn[['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']].mean(axis=1)
stockyearlyreturn['SUM'] = stockyearlyreturn[['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']].sum(axis=1)
print(stockyearlyreturn)

### Get the best year with the best performance considering either:
####-the average return of the portfolio
####-the sum of the return if the investor is doing diversification and has all the stocks
####-the stock within the maximum return in our stocklist
max = [stockyearlyreturn['MAX'], stockyearlyreturn['MEAN'], stockyearlyreturn['SUM']]
for i in max:
    a = i.max()
    row = stockyearlyreturn.loc[i == a]
    year = row.iloc[0,0]
    print(year)     #The best year was 2016 for diversificated investment

# Modelisation of the same number of investors but with an initial amount driven from a normal distribution
datelist = MyDataframe['Date'].tolist()  #Choose a random date for the investment

defensiveinvestors = []
aggressiveinvestors = []
mixedinvestors = []
models = (500)

for i in range(models):
    rstartdate = random.choice(datelist)
    startdateindex = datelist.index(rstartdate)
    enddatelist = datelist[startdateindex:]
    renddate = random.choice(enddatelist)
    initialamount = random.normalvariate(mu=20000, sigma=5000)
    defensiveinvestors.append(defensivereturn(initialamount, rstartdate, renddate))
    aggressiveinvestors.append(aggressivereturn(initialamount, rstartdate, renddate))
    mixedinvestors.append(mixedreturn(0.25, 0.75, initialamount, rstartdate, renddate))

## Plot the modelisation in order to analyse it
fig = plt.figure(7)
plt.plot(defensiveinvestors, 'b-',
         aggressiveinvestors, 'r-',
         mixedinvestors, 'g-')
plt.xlabel('Models')
plt.ylabel('Return')
plt.title('Standard distributed initial amount')
plt.grid(True)
plt.show()
fig.savefig(os.path.abspath(("../Results/Standard distributed initial amount.png")))

# The best stock in 2017
stockreturn2017 = []
stockreturn2017.append(stockreturn('FDX', '2017-01-03', '2017-12-29'))
stockreturn2017.append(stockreturn('GOOGL', '2017-01-03', '2017-12-29'))
stockreturn2017.append(stockreturn('XOM', '2017-01-03', '2017-12-29'))
stockreturn2017.append(stockreturn('KO', '2017-01-03', '2017-12-29'))
stockreturn2017.append(stockreturn('NOK', '2017-01-03', '2017-12-29'))
stockreturn2017.append(stockreturn('MS', '2017-01-03', '2017-12-29'))
stockreturn2017.append(stockreturn('IBM', '2017-01-03', '2017-12-29'))


stock2017 =pd.DataFrame({'Stocks': stocklist,
                        'Return': stockreturn2017})
print(stock2017)

max2017 = stock2017['Return'].max()
stockrow = stock2017.loc[stock2017['Return'] == max2017 ]
stock = stockrow.iloc[0,0]
print(stock)    #The best stock in 2017 was FedEx with a yearly performance of 32.18%



