# Student 1: M2 FIT - Maminiaina Alexandra Ramilison
# Student 2: M2 FIT - Massari Sousa Pontes

# PART 1: INVESTMENT USING SHORT AND LONG TERM BONDS
import matplotlib.pyplot as plt
import os

# Variables
STIA = 250  # Short term bonds minimum investment
STMT = 2  # Minimum term for short term bonds
STIR = 0.015  # Short term interest rate p.a.
LTIA = 1000  # Long term bonds minimum investment
LTMT = 5  # Minimum term for long term bonds
LTIR = 0.03  # Long term interest rate p.a.
n = 50  # Term of investment in nb of years
nbonds = 1

# Global function for investment return
def streturn(nbonds, initialamount, term):
    streturn = nbonds * (initialamount * (1 + STIR) ** term)
    return streturn

def ltreturn(nbonds, initialamount, term):
    ltreturn = nbonds * (initialamount * (1 + LTIR) ** term)
    return ltreturn

# Calculation of investment return per year for 50 years
shorttermbondreturn = []
longtermbondreturn = []

STterm = [i for i in range(0, n)]
LTterm = [j for j in range(0, n)]

for i in STterm:
    shorttermbondreturn.append(streturn(nbonds, STIA, i))

for j in LTterm:
    longtermbondreturn.append(ltreturn(nbonds, LTIA, j))

# Figures plot
fig = plt.figure(1)
plt.plot(STterm, shorttermbondreturn, 'b-',
         LTterm, longtermbondreturn, 'ro-')
plt.xlabel('Term')
plt.ylabel('Investment return')
plt.title('Investment return on bonds')
plt.grid(True)
plt.show()
fig.savefig(os.path.abspath("../Results/InvestmentReturn.png"))
