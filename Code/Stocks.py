# Student 1: M2 FIT - Maminiaina Alexandra Ramilison
# Student 2: M2 FIT - Massari Sousa Pontes

# PART 2: INVESTMENT USING STOCKS

import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import datetime as dt
import random

inputdataset = os.path.abspath("../Data/StocksData.csv")
MyDataframe = pd.read_csv(inputdataset, sep=",")

# Plot the values for every stock
fig2 = MyDataframe.plot()
plt.xlabel('Time')
plt.ylabel('Stocks value')
plt.title('Investment on stocks')
plt.show()
fig2.figure.savefig(os.path.abspath("../Results/Stocks Value Evolution.png"))

#Define a function which will return the return on investment
def get_column(name):
    if (name == "FDX"):
        return 1
    elif (name == "GOOGL"):
        return 2
    elif (name == "XOM"):
        return 3
    elif (name == "KO"):
        return 4
    elif (name == "NOK"):
        return 5
    elif (name == "MS"):
        return 6
    elif (name == "IBM"):
        return 7

def stockreturn(nstocks, name, startdate, enddate):
    c = get_column(name)
    rp = MyDataframe.loc[MyDataframe['Date'] == startdate]
    purchaseprice = rp.iloc[0,c]
    rs = MyDataframe.loc[MyDataframe['Date'] == enddate]
    saleprice = rs.iloc[0,c]
    stockreturn = nstocks * purchaseprice * (1+ (saleprice - purchaseprice)/purchaseprice)
    return (stockreturn)

print (stockreturn(1, 'FDX', '2016-07-06', '2017-04-03'))







