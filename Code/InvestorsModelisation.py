# Student 1: M2 FIT - Maminiaina Alexandra Ramilison
# Student 2: M2 FIT - Massari Sousa Pontes

# PART 3: INVESTMENT MODELISATION

from Bonds import streturn, ltreturn
from Stocks import stockreturn, get_column
from datetime import date
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import datetime as dt
import random

# Variables
STIA = 250  # Short term bonds minimum investment
STMT = 2  # Minimum term for short term bonds
STIR = 0.015  # Short term interest rate p.a.
LTIA = 1000  # Long term bonds minimum investment
LTMT = 5  # Minimum term for long term bonds
LTIR = 0.03  # Long term interest rate p.a.
stocklist = ['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']

# Import data file
inputdataset = os.path.abspath("../Data/StocksData.csv")
MyDataframe = pd.read_csv(inputdataset, sep=",")

## Modelisation of defensive investor
bonds = 0.5 #proportion of short term bonds
print (bonds)

def shorttermbonds(bonds, initialamount, startdate, enddate):
    start = date(*map(int, startdate.split('-')))
    end = date(*map(int, enddate.split('-')))
    term = (end - start).days / 365
    if term >= 2:
        if initialamount - bonds*initialamount >= 250:
            return (streturn (bonds, initialamount, term))
        if initialamount - bonds*initialamount < 250:
            return (streturn((250/initialamount), initialamount, term))
    if term < 2:
        return 0

print (shorttermbonds(bonds, 10000, '2016-06-09', '2020-12-30'))

def longtermbonds(bonds, initialamount, startdate, enddate):
    start = date(*map(int, startdate.split('-')))
    end = date(*map(int, enddate.split('-')))
    term = (end - start).days / 365
    if term >= 5:
        if initialamount - bonds*initialamount >= 250:
            return (ltreturn((1-bonds), initialamount, term))
        if initialamount - bonds*initialamount < 250:
            return (ltreturn((1 - 250/initialamount), initialamount, term))
    elif term < 5:
        return (shorttermbonds(1-bonds, initialamount, startdate, enddate))

def defensivereturn(initialamount, startdate, enddate):
    defreturn = shorttermbonds(bonds, initialamount, startdate, enddate) + longtermbonds(bonds, initialamount, startdate, enddate)
    return defreturn

print (defensivereturn(10000, '2016-06-09', '2020-12-30'))

## Modelisation of aggressive investors
def aggressivereturn (initialamount, startdate, enddate):
    stock = random.choice(stocklist)
    c = get_column(stock)
    rp = MyDataframe.loc[MyDataframe['Date'] == startdate]  #Select the row corresponding to the date
    purchaseprice = rp.iloc[0, c]   # Get the purchase price of the stock
    maxnstocks = int((initialamount - 100) / purchaseprice) #Calculate the maximum nb of stocks
    nstocks = random.randint(0, maxnstocks) #Get a random number of stocks the investor can buy
    remainingbudget = initialamount - nstocks * purchaseprice
    aggreturn = stockreturn(nstocks, stock, startdate, enddate) + remainingbudget
    return aggreturn

print(aggressivereturn(10000, '2016-06-09', '2020-12-30'))

## Modelisation of mixed investor
def mixedreturn(pbonds, pstocks, initialamount, startdate, enddate):
    inbonds = pbonds * initialamount
    instocks = pstocks * initialamount
    start = date(*map(int, startdate.split('-')))
    end = date(*map(int, enddate.split('-')))
    term = (end - start).days / 365 # term in number of years
    mixedreturn = defensivereturn(inbonds, startdate, enddate) + aggressivereturn(instocks, startdate, enddate)
    return mixedreturn

print(mixedreturn(0.25, 0.75, 10000, '2016-08-04', '2018-02-15'))

## Modelisation of 500 of each investors
datelist = MyDataframe['Date'].tolist()  #Choose a random date for the investment

defensiveinvestors = []
aggressiveinvestors = []
mixedinvestors = []
models = (500)

for i in range(models):
    rstartdate = random.choice(datelist)
    startdateindex = datelist.index(rstartdate)
    enddatelist = datelist[startdateindex:]
    renddate = random.choice(enddatelist)

    defensiveinvestors.append(defensivereturn(5000, rstartdate, renddate))
    aggressiveinvestors.append(aggressivereturn(5000, rstartdate, renddate))
    mixedinvestors.append(mixedreturn(0.25, 0.75, 5000, rstartdate, renddate))

## Plot the modelisation in order to analyse it
fig = plt.figure(3)
plt.plot(defensiveinvestors, 'b-',
         aggressiveinvestors, 'r-',
         mixedinvestors, 'g-')
plt.xlabel('Models')
plt.ylabel('Return')
plt.title('500 Investors modelling')
plt.grid(True)
plt.show()
fig.savefig(os.path.abspath(("../Results/Investors Modeling.png")))














