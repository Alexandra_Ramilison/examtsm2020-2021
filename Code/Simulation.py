# Student 1: M2 FIT - Maminiaina Alexandra Ramilison
# Student 2: M2 FIT - Massari Lima Pontes

# PART 4: SIMULATION

from InvestorsModelisation import shorttermbonds, longtermbonds, defensivereturn, aggressivereturn, mixedreturn
from Bonds import streturn, ltreturn
from Stocks import stockreturn, get_column
from datetime import date
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import datetime
import random

# Variables
STIA = 250  # Short term bonds minimum investment
STMT = 2  # Minimum term for short term bonds
STIR = 0.015  # Short term interest rate p.a.
LTIA = 1000  # Long term bonds minimum investment
LTMT = 5  # Minimum term for long term bonds
LTIR = 0.03  # Long term interest rate p.a.
ia = 5000 # Initial amount
stocklist = ['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']

# Import data file
inputdataset = os.path.abspath("../Data/StocksData.csv")
MyDataframe = pd.read_csv(inputdataset, sep=",")

## Modelisation of 500 investors from 2016 to 2020
pbonds = random.randint(0,100)/100
datelist = MyDataframe['Date'].tolist()  #Choose a random date for the investment

year = []
defensiveinvestors = []
aggressiveinvestors = []
mixedinvestors = []
models1 = (100)
models2 = (100)
models3 = (100)
models4 = (100)
models5 = (100)

### Model 100 investors per year with a start date staying the same
#### Return for 1 year of investment in 2016
for i in range(models1):
    year.append('2016')
    defensiveinvestors.append(defensivereturn(ia, '2016-01-11', '2016-12-30'))
    aggressiveinvestors.append(aggressivereturn(ia, '2016-01-11', '2016-12-30'))
    mixedinvestors.append(mixedreturn(0.25, 0.75, ia, '2016-01-11', '2016-12-30'))

#### Return for 2 years of investment from 2016 to 2017
for i in range(models2):
    year.append('2017')
    defensiveinvestors.append(defensivereturn(ia, '2016-01-11', '2017-12-29'))
    aggressiveinvestors.append(aggressivereturn(ia, '2016-01-11', '2017-12-29'))
    mixedinvestors.append(mixedreturn(0.25, 0.75, ia, '2016-01-11', '2017-12-29'))

#### Return for 3 years of investment from 2016 to 2018
for i in range(models3):
    year.append('2018')
    defensiveinvestors.append(defensivereturn(ia, '2016-01-11', '2018-12-31'))
    aggressiveinvestors.append(aggressivereturn(ia, '2016-01-11', '2018-12-31'))
    mixedinvestors.append(mixedreturn(0.25, 0.75,ia, '2016-01-11', '2018-12-31'))

#### Return for 2 years of investment from 2016 to 2019
for i in range(models4):
    year.append('2019')
    defensiveinvestors.append(defensivereturn(ia, '2016-01-11', '2019-12-31'))
    aggressiveinvestors.append(aggressivereturn(ia, '2016-01-11', '2019-12-31'))
    mixedinvestors.append(mixedreturn(0.25, 0.75,ia, '2016-01-11', '2019-12-31'))

#### Return for 2 years of investment from 2016 to 2020
for i in range(models5):
    year.append('2020')
    defensiveinvestors.append(defensivereturn(ia, '2016-01-11', '2020-12-31'))
    aggressiveinvestors.append(aggressivereturn(ia, '2016-01-11', '2020-12-31'))
    mixedinvestors.append(mixedreturn(0.25, 0.75, ia, '2016-01-11', '2020-12-31'))

investorsmodelisation = pd.DataFrame({'Year': year,
                                      'Defensive': defensiveinvestors,
                                      'Aggressive': aggressiveinvestors,
                                      'Mixed': mixedinvestors})

print(investorsmodelisation)

defensivemean = []
aggressivemean =[]
mixedmean = []

### Create a dataframe with the yearly mean for each type of investors
years = ['2016', '2017', '2018', '2019', '2020']
for i in years:
    investors = investorsmodelisation[investorsmodelisation['Year'] == i]
    defensivemean.append(investors['Defensive'].mean())
    aggressivemean.append(investors['Aggressive'].mean())
    mixedmean.append(investors['Mixed'].mean())

meanreturn = pd.DataFrame({'Year': years,
                           'Defensive': defensivemean,
                           'Aggressive': aggressivemean,
                           'Mixed': mixedmean})
print(meanreturn)

### Plot the values for every year
fig3 = meanreturn.plot()
plt.xlabel('Year')
plt.ylabel('Return')
plt.title('Investors yearly return from 2016 to 2020')
plt.show()
fig3.figure.savefig(os.path.abspath("../Results/Investors Yearly Return.png"))

## Mixed investors redistribute their budget when the bond period ends
#### Return for 1 year of investment in 2016
year1 = []
defensiveinvestors1 = []
aggressiveinvestors1 = []
mixedinvestors1 = []

for i in range(models1):
    year1.append('2016')
    defensiveinvestors1.append(defensivereturn(ia, '2016-01-11', '2016-12-30'))
    aggressiveinvestors1.append(aggressivereturn(ia, '2016-01-11', '2016-12-30'))
    mixedinvestors1.append(mixedreturn(0.25, 0.75, ia, '2016-01-11', '2016-12-30'))

#### Return for 2 years of investment from 2016 to 2017
for i in range(models2):
    year1.append('2017')
    defensiveinvestors1.append(defensivereturn(ia, '2016-01-11', '2017-12-29'))
    aggressiveinvestors1.append(aggressivereturn(ia, '2016-01-11', '2017-12-29'))
    mixedinvestors1.append(mixedreturn(0.25, 0.75, ia, '2016-01-11', '2017-12-29'))

#### Return for 3 years of investment from 2016 to 2018
for i in range(models3):
    year1.append('2018')
    defensiveinvestors1.append(defensivereturn(ia, '2016-01-11', '2018-12-31'))
    aggressiveinvestors1.append(aggressivereturn(ia, '2016-01-11', '2018-12-31'))
    mixedinvestors1.append(mixedreturn(0.25, 0.75, ia, '2016-01-11', '2017-12-29') +
                          mixedreturn(0.25, 0.75, ia, '2017-12-29', '2018-12-31'))

#### Return for 2 years of investment from 2016 to 2019
for i in range(models4):
    year1.append('2019')
    defensiveinvestors1.append(defensivereturn(ia, '2016-01-11', '2019-12-31'))
    aggressiveinvestors1.append(aggressivereturn(ia, '2016-01-11', '2019-12-31'))
    mixedinvestors1.append(mixedreturn(0.25, 0.75, ia, '2016-01-11', '2017-12-29') +
                          mixedreturn(0.25, 0.75, ia, '2017-12-29', '2019-12-31'))

#### Return for 2 years of investment from 2016 to 2020
for i in range(models5):
    year1.append('2020')
    defensiveinvestors1.append(defensivereturn(ia, '2016-01-11', '2020-12-31'))
    aggressiveinvestors1.append(aggressivereturn(ia, '2016-01-11', '2020-12-31'))
    mixedinvestors1.append(mixedreturn(0.25, 0.75, ia, '2016-01-11', '2017-12-29') +
                          mixedreturn(0.25, 0.75,ia, '2017-12-29', '2019-12-31') +
                          mixedreturn(0.25, 0.75, ia, '2019-12-31', '2020-12-31'))

investorsmodelisation1 = pd.DataFrame({'Year': year1,
                                      'Defensive': defensiveinvestors1,
                                      'Aggressive': aggressiveinvestors1,
                                      'Mixed': mixedinvestors1})

print(investorsmodelisation1)

defensivemean1 = []
aggressivemean1 =[]
mixedmean1 = []

### Create a dataframe with the yearly mean for each type of investors
years = ['2016', '2017', '2018', '2019', '2020']
for i in years:
    investors = investorsmodelisation1[investorsmodelisation1['Year'] == i]
    defensivemean1.append(investors['Defensive'].mean())
    aggressivemean1.append(investors['Aggressive'].mean())
    mixedmean1.append(investors['Mixed'].mean())

meanreturn1 = pd.DataFrame({'Year': years,
                           'Defensive': defensivemean1,
                           'Aggressive': aggressivemean1,
                           'Mixed': mixedmean1})
print(meanreturn1)

### Plot the values for every year
fig4 = meanreturn1.plot()
plt.xlabel('Year')
plt.ylabel('Return')
plt.title('Reinvested Investors Yearly Return')
plt.show()
fig4.figure.savefig(os.path.abspath("../Results/Reinvested Investors Yearly Return.png"))

###Switching to 75-25 bonds vs stocks
#### Return for 1 year of investment in 2016
year2 = []
defensiveinvestors2 = []
aggressiveinvestors2 = []
mixedinvestors2 = []

for i in range(models1):
    year2.append('2016')
    defensiveinvestors2.append(defensivereturn(ia, '2016-01-11', '2016-12-30'))
    aggressiveinvestors2.append(aggressivereturn(ia, '2016-01-11', '2016-12-30'))
    mixedinvestors2.append(mixedreturn(0.75, 0.25, ia, '2016-01-11', '2016-12-30'))

#### Return for 2 years of investment from 2016 to 2017
for i in range(models2):
    year2.append('2017')
    defensiveinvestors2.append(defensivereturn(ia, '2016-01-11', '2017-12-29'))
    aggressiveinvestors2.append(aggressivereturn(ia, '2016-01-11', '2017-12-29'))
    mixedinvestors2.append(mixedreturn(0.75, 0.25, ia, '2016-01-11', '2017-12-29'))

#### Return for 3 years of investment from 2016 to 2018
for i in range(models3):
    year2.append('2018')
    defensiveinvestors2.append(defensivereturn(ia, '2016-01-11', '2018-12-31'))
    aggressiveinvestors2.append(aggressivereturn(ia, '2016-01-11', '2018-12-31'))
    mixedinvestors2.append(mixedreturn(0.75, 0.25, ia, '2016-01-11', '2017-12-29') +
                          mixedreturn(0.75, 0.25, ia, '2017-12-29', '2018-12-31'))

#### Return for 2 years of investment from 2016 to 2019
for i in range(models4):
    year2.append('2019')
    defensiveinvestors2.append(defensivereturn(ia, '2016-01-11', '2019-12-31'))
    aggressiveinvestors2.append(aggressivereturn(ia, '2016-01-11', '2019-12-31'))
    mixedinvestors2.append(mixedreturn(0.75, 0.25, ia, '2016-01-11', '2017-12-29') +
                          mixedreturn(0.75, 0.25, ia, '2017-12-29', '2019-12-31'))

for i in range(models5):
    year2.append('2020')
    defensiveinvestors2.append(defensivereturn(ia, '2016-01-11', '2020-12-31'))
    aggressiveinvestors2.append(aggressivereturn(ia, '2016-01-11', '2020-12-31'))
    mixedinvestors2.append(mixedreturn(0.75, 0.25, ia, '2016-01-11', '2017-12-29') +
                          mixedreturn(0.75, 0.25,ia, '2017-12-29', '2019-12-31') +
                          mixedreturn(0.75, 0.25, ia, '2019-12-31', '2020-12-31'))

investorsmodelisation2 = pd.DataFrame({'Year': year2,
                                      'Defensive': defensiveinvestors2,
                                      'Aggressive': aggressiveinvestors2,
                                      'Mixed': mixedinvestors2})

print(investorsmodelisation2)

defensivemean2 = []
aggressivemean2 =[]
mixedmean2 = []

### Create a dataframe with the yearly mean for each type of investors
years = ['2016', '2017', '2018', '2019', '2020']
for i in years:
    investors = investorsmodelisation2[investorsmodelisation2['Year'] == i]
    defensivemean2.append(investors['Defensive'].mean())
    aggressivemean2.append(investors['Aggressive'].mean())
    mixedmean2.append(investors['Mixed'].mean())

meanreturn2 = pd.DataFrame({'Year': years,
                           'Defensive': defensivemean2,
                           'Aggressive': aggressivemean2,
                           'Mixed': mixedmean2})
print(meanreturn2)

### Plot the values for every year
fig5 = meanreturn2.plot()
plt.xlabel('Year')
plt.ylabel('Return')
plt.title('75-25 Investors Yearly Return')
plt.show()
fig5.figure.savefig(os.path.abspath("../Results/75-25 Investors Yearly Return.png"))

#Multiplying all starting budgets by 10
ia1 = 50000
#### Return for 1 year of investment in 2016
year3 = []
defensiveinvestors3 = []
aggressiveinvestors3 = []
mixedinvestors3 = []

for i in range(models1):
    year3.append('2016')
    defensiveinvestors3.append(defensivereturn(ia1, '2016-01-11', '2016-12-30'))
    aggressiveinvestors3.append(aggressivereturn(ia1, '2016-01-11', '2016-12-30'))
    mixedinvestors3.append(mixedreturn(0.25, 0.75, ia1, '2016-01-11', '2016-12-30'))

#### Return for 2 years of investment from 2016 to 2017
for i in range(models2):
    year3.append('2017')
    defensiveinvestors3.append(defensivereturn(ia1, '2016-01-11', '2017-12-29'))
    aggressiveinvestors3.append(aggressivereturn(ia1, '2016-01-11', '2017-12-29'))
    mixedinvestors3.append(mixedreturn(0.25, 0.75, ia1, '2016-01-11', '2017-12-29'))

#### Return for 3 years of investment from 2016 to 2018
for i in range(models3):
    year3.append('2018')
    defensiveinvestors3.append(defensivereturn(ia1, '2016-01-11', '2018-12-31'))
    aggressiveinvestors3.append(aggressivereturn(ia1, '2016-01-11', '2018-12-31'))
    mixedinvestors3.append(mixedreturn(0.25, 0.75, ia1, '2016-01-11', '2017-12-29') +
                          mixedreturn(0.25, 0.75, ia1, '2017-12-29', '2018-12-31'))

#### Return for 2 years of investment from 2016 to 2019
for i in range(models4):
    year3.append('2019')
    defensiveinvestors3.append(defensivereturn(ia1, '2016-01-11', '2019-12-31'))
    aggressiveinvestors3.append(aggressivereturn(ia1, '2016-01-11', '2019-12-31'))
    mixedinvestors3.append(mixedreturn(0.25, 0.75, ia1, '2016-01-11', '2017-12-29') +
                          mixedreturn(0.25, 0.75, ia1, '2017-12-29', '2019-12-31'))

for i in range(models5):
    year3.append('2020')
    defensiveinvestors3.append(defensivereturn(ia1, '2016-01-11', '2020-12-31'))
    aggressiveinvestors3.append(aggressivereturn(ia1, '2016-01-11', '2020-12-31'))
    mixedinvestors3.append(mixedreturn(0.25, 0.75, ia1, '2016-01-11', '2017-12-29') +
                          mixedreturn(0.25, 0.75, ia1, '2017-12-29', '2019-12-31') +
                          mixedreturn(0.25, 0.75, ia1, '2019-12-31', '2020-12-31'))

investorsmodelisation3 = pd.DataFrame({'Year': year3,
                                      'Defensive': defensiveinvestors3,
                                      'Aggressive': aggressiveinvestors3,
                                      'Mixed': mixedinvestors3})

print(investorsmodelisation3)

defensivemean3 = []
aggressivemean3 =[]
mixedmean3 = []

### Create a dataframe with the yearly mean for each type of investors
years = ['2016', '2017', '2018', '2019', '2020']
for i in years:
    investors = investorsmodelisation3[investorsmodelisation3['Year'] == i]
    defensivemean3.append(investors['Defensive'].mean())
    aggressivemean3.append(investors['Aggressive'].mean())
    mixedmean3.append(investors['Mixed'].mean())

meanreturn3 = pd.DataFrame({'Year': years,
                           'Defensive': defensivemean3,
                           'Aggressive': aggressivemean3,
                           'Mixed': mixedmean3})
print(meanreturn3)

### Plot the values for every year
fig6 = meanreturn3.plot()
plt.xlabel('Year')
plt.ylabel('Return')
plt.title('New budget Investors Yearly Return with reinvestment')
plt.show()
fig6.figure.savefig(os.path.abspath("../Results/New budget Investors Yearly Return with reinvestment.png"))



